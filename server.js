import fs from 'fs';

import express from 'express';
const app = express();

import open from 'open';

import bodyParser from 'body-parser';

//DIY components
import Member from './model.js';

//middleware

app.use(bodyParser.urlencoded({ extended: false }));




app.use(express.static(__dirname + '/public'));



/************************routes************************/
var members = {
    '100': new Member('100', 'AA ABC'),
    '101': new Member('101', 'BB BCD')
};

//Get all members
app.get('/api/members', (req, res) => {
    res.json( { data: members } );
});

//Get a member
app.get('/api/member/:id', (req, res) => {
    var member = members[req.params.id];
    if(member) {
        res.json( { data: member } );
    }
    else {
        res.status(404).json('Invalid member id.');
    }
});

//Add a member
app.post('/api/addmember', (req, res) => {
    var toAddMember = req.body;
    members[toAddMember.id] = toAddMember;

    fs.writeFile(__dirname + '/members.json', JSON.stringify(members), (err) => {
        if(err) {
            console.log(err);
        }
        else {
            console.log(members);
            console.log("The file was saved!");
        }
    });

    res.status(201).json( {data: toAddMember } );
});

//catch 404 and forwarding to error handler
app.get('*', (req, res, next) => {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// app.use((req, res, next) => {
//     let err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });

//error handler
app.use((err, req, res, next) => {
    res.status(err.status || 500).send(err.message);
    next(err);
});


//Default listening 3001
const serverPort = process.env.SERVER_PORT || 3001;
app.listen(serverPort, () => {
    console.log(`Listening port ${serverPort}`);
    open(`http://localhost:${serverPort}`, 'Google Chrome');
});

