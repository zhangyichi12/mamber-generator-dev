$(() => {
   // $.get('/api/members', (members) => {
    $.get('localhost:3001/api/members', (members) => {
        var list = [];
        for(var i in members.data) {
            var content = `<a class="member-item list-group-item text-center"
                              href="/api/member/${members.data[i].id}">
                              ID: ${members.data[i].id}    
                              Name: ${members.data[i].name}
                           </a>`;
            list.push($('<li>', { html: content }));
        }
        $('.member-list').append(list);
    });

    $('form').on('submit', (event) => {
        event.preventDefault();
        var form = $('form');
        var memberData = form.serialize();

        $.ajax({
            type: 'post',
            url: '/api/addmember',
            data: memberData
        }).done((addedMember) => {
            var content = `<a class="list-group-item text-center"
                              href="/api/member/${addedMember.data.id}">
                              ID: ${addedMember.data.id}    
                              Name: ${addedMember.data.name}
                           </a>`;
            $('.member-list').append(content);
            form.trigger('reset');
        });
    });
});